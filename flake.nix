{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, utils }:
    utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages."${system}";
    in rec {
      packages.hagrid = pkgs.callPackage ./. { };
      packages.wkdDomainChecker = pkgs.callPackage ./wkd-domain-checker/. { };

      packages.default = packages.hagrid;
    }) // {
      overlays.hagrid = (final: prev: { hagrid = self.packages."${final.system}".hagrid; });
      overlays.wkdDomainChecker = (final: prev: { wkdDomainChecker = self.packages."${final.system}".wkdDomainChecker; });

      overlays.default = self.overlays.hagrid;
    };
}
