#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
  name='wkd-domain-checker',
  version='1.0',
  # Modules to import from other scripts:
  packages=find_packages(),
  # Executables
  scripts=["wkd-domain-checker.py"],
)
